# Natural Language Toolkit: List Sorting
#
# Copyright (C) 2001-2010 NLTK Project
# Author: Steven Bird <sb@csse.unimelb.edu.au>
# URL: <http://www.nltk.org/>
# For license information, see LICENSE.TXT

"""
This module provides a variety of list sorting algorithms, to
illustrate the many different algorithms (recipes) for solving a
problem, and how to analyze algorithms experimentally.
"""

# These algorithms are taken from:
# Levitin (2004) The Design and Analysis of Algorithms


##################################################################
# Selection Sort
##################################################################

def selection(a)
    """
    Selection Sort: scan the list to find its smallest element, then
    swap it with the first element.  The remainder of the list is one
    element smaller; apply the same method to this list, and so on.
    """
    count = 0

    for i in (0)...(a.size() - 1)
        min = i

        for j in (i + 1)...(a.size())
            if a[j] < a[min]
                min = j

            end
            count += 1

        end
        a[min], a[i] = a[i], a[min]

    end
    return count

end
##################################################################
# Bubble Sort
##################################################################

def bubble(a)
    """
    Bubble Sort: compare adjacent elements of the list left-to-right,
    and swap them if they are out of order.  After one pass through
    the list swapping adjacent items, the largest item will be in
    the rightmost position.  The remainder is one element smaller;
    apply the same method to this list, and so on.
    """
    count = 0
    for i in (0)...(a.size() - 1)
        for j in (0)...(a.size() - i - 1)
            if a[j + 1] < a[j]
                a[j], a[j + 1] = a[j + 1], a[j]
                count += 1
            end
        end
    end
    return count


end
##################################################################
# Merge Sort
##################################################################

def _merge_lists(b, c)
    count = 0
    i = j = 0
    a = []
    while (i < b.size() && j < c.size())
        count += 1
        if b[i] <= c[j]
            a.push(b[i])
            i += 1
        else
            a.push(c[j])
            j += 1
        end
    end
    if i == b.size()
        a += c[j..-1]
    else
        a += b[i..-1]
    end
    return a, count

end
def merge(a)
    """
    Merge Sort: split the list in half, and sort each half, then
    combine the sorted halves.
    """
    count = 0
    if a.size() > 1
        midpoint = a.size() / 2
        b = a.first(midpoint)
        c = a[midpoint..-1]
        count_b = merge(b)
        count_c = merge(c)
        result, count_a = _merge_lists(b, c)
        a[0..- 1] = result # copy the result back into a.
        count = count_a + count_b + count_c
    end
    return count

end
##################################################################
# Quick Sort
##################################################################

def _partition(a, l, r)
    p = a[l]; i = l; j = r + 1
    count = 0
    while true
        while i < r
            i += 1
            break if a[i] >= p
        end
        while j > l
            j -= 1
            break if j < l || a[j] <= p
        end
        a[i], a[j] = a[j], a[i] # swap
        count += 1
        break if i >= j
    end
    a[i], a[j] = a[j], a[i] # undo last swap
    a[l], a[j] = a[j], a[l]
    return j, count

end
def _quick(a, l, r)
    count = 0
    if l < r
        s, count = _partition(a, l, r)
        count += _quick(a, l, s - 1)
        count += _quick(a, s + 1, r)
    end
    return count

end
def quick(a)
    return _quick(a, 0, a.size() - 1)

end
##################################################################
# Demonstration
##################################################################

def demo()

    for size in [10, 20, 50, 100, 200, 500, 1000]
        a = ((0)...(size)).to_a

        # various sort methods
        a.shuffle(); count_selection = selection(a)
        a.shuffle(); count_bubble = bubble(a)
        a.shuffle(); count_merge = merge(a)
        a.shuffle(); count_quick = quick(a)

        puts (("size=%5d:  selection=%8d,  bubble=%8d,  " \
                "merge=%6d,  quick=%6d") %
            [size, count_selection, count_bubble,
                count_merge, count_quick])

    end
end
if __FILE__ == $0
    demo()
end

