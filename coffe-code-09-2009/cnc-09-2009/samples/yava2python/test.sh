#!/bin/sh

found=`ls  java`

for f in ${found}; do 
    txl ./java/${f} > ./tmp.py
    echo "**************************"
    echo "Transforming ${f}" 
    echo "**************************"
    python ./tmp.py
done

rm ./tmp.py