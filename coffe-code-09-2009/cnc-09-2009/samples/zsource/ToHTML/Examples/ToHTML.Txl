% Generic web page maker for source in any language or text
% Jim Cordy, June 1999

% This generic program will mark up source programs in any language,
% given the language's TXL grammar, including formatting, keywords,
% comments and token definitions.  It works for both raw text source
% (line grammars) and formatted output source (full parsing grammars).

% This generic partial program should be used by a main program of the form:
%
%        include "Language.Grammar"
%        include "ToHTML.Txl"
%
 
% HTML grammar and standard function definitions 
include "HTML.Module"

% OK, let's webify!
function main
    replace [any]
        Program [any]
        
    % Tags to wrap the output in
    construct HTMLTag [html_begin]
        <HTML>
    construct PreTag [html_begin]
        <PRE>
        
    % Characters that must be encoded in HTML ...
    construct Tokens [repeat token]
        '< '>
    % ... and their corresponding encodings
    construct HTMLTokens [repeat stringlit]
        "&lt;" "&gt;"
    by
        Program    
            [fixtokens each Tokens HTMLTokens]    % fix < and > signs, etc.
            [fixstrs each Tokens HTMLTokens]      % fix < and > signs, etc. in string literals
            [fixchars each Tokens HTMLTokens]     % fix < and > signs, etc. in char literals
            [fixcomments each Tokens HTMLTokens]  % fix < and > signs, etc. in comments
            [addbreaks]                           % add line breaks for blank lines
            [keycolor  "blue"]                    % make keywords blue
            [commcolor "green"]                   % make comments green
            [strcolor  "red"]                     % make stringlits red
            [charcolor "red"]                     % make charlits red
            [tagwith PreTag]                      % <PRE> Program </PRE>
            [tagwith HTMLTag]                     % <HTML><PRE> Program </PRE></HTML>                    
end function

% Colorize every keyword of the language
rule keycolor Color [stringlit]
    skipping [html_item]
    replace $ [key]
        Keyword [key]
    by
        Keyword [colorize Color]
end rule

% Colorize every comment of the language
rule commcolor Color [stringlit]
    skipping [html_item]
    replace $ [comment]
        Comment [comment]
    by
        Comment [colorize Color]
end rule

% Colorize every character literal
rule charcolor Color [stringlit]
    skipping [html_item]
    replace $ [charlit]
        Literal [charlit]
    by
        Literal [colorize Color]
end rule

% Colorize every string literal
rule strcolor Color [stringlit]
    skipping [html_item]
    replace $ [stringlit]
        Literal [stringlit]
    by
        Literal [colorize Color]
end rule

% Encode each occurence of an illegal HTML token with its encoding
rule fixtokens T [token] HtmlOfT [stringlit]
    replace $ [token]
        T
    construct HtmlT [id]
        _ [unquote HtmlOfT]
    by
        T [changeto HtmlT]
end rule

function changeto Id [id]
    replace [any]
        A [any]
    deconstruct Id
        AId [any]
    by
        AId
end function

% Same applies inside string literals
rule fixstrs T [token] HtmlOfT [stringlit]
    construct ST [stringlit]
        _ [quote T]
    replace $ [stringlit]
        S [stringlit]
    construct Tindex [number]
        _ [index S ST]
    deconstruct not Tindex
        0
    construct TindexMinus1 [number]
        Tindex [- 1]
    construct TindexPlus1 [number]
        Tindex [+ 1]
    construct PreST [stringlit]
        S [: 1 TindexMinus1]
    construct PostST [stringlit]
        S [: TindexPlus1 9999] [fixstrs T HtmlOfT]
    by
        PreST [+ HtmlOfT] [+ PostST]
end rule

% And inside character literals
rule fixchars T [token] HtmlOfT [stringlit]
    construct ST [charlit]
        _ [quote T]
    replace $ [charlit]
        S [charlit]
    construct Tindex [number]
        _ [index S ST]
    deconstruct not Tindex
        0
    construct TindexMinus1 [number]
        Tindex [- 1]
    construct TindexPlus1 [number]
        Tindex [+ 1]
    construct PreST [charlit]
        S [: 1 TindexMinus1]
    construct PostST [charlit]
        S [: TindexPlus1 9999] [fixchars T HtmlOfT]
    by
        PreST [+ HtmlOfT] [+ PostST]
end rule

% And inside comments
rule fixcomments T [token] HtmlOfT [stringlit]
    construct ST [stringlit]
        _ [quote T]
    replace $ [comment]
        S [comment]
    construct Tindex [number]
        _ [index S ST]
    deconstruct not Tindex
        0
    construct TindexMinus1 [number]
        Tindex [- 1]
    construct TindexPlus1 [number]
        Tindex [+ 1]
    construct PreST [comment]
        S [: 1 TindexMinus1]
    construct PostST [comment]
        S [: TindexPlus1 9999] [fixcomments T HtmlOfT]
    by
        PreST [+ HtmlOfT] [+ PostST]
end rule

% Add line breaks for blank lines
rule addbreaks
    replace [line]
        NL [newline]
    by
        <BR> NL
end rule
