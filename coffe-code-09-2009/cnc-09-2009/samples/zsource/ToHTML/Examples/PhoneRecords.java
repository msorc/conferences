// The "PhoneRecords" program - demonstrates the use
// of a Java class to implement a record data type.
import java.awt.*;
import hsa.Console;

public class PhoneRecords
{
    static Console c;           // The output console

    // a record data type for phonebook entries
    static class PhoneEntry
    {
        String name;
        int number;
    }


    public static void main (String [] args)
    {
        c = new Console ();

        // 1. Example of a single phone record

        PhoneEntry ProfInfo = new PhoneEntry ();

        ProfInfo.name = "Prof. Jim Cordy";
        ProfInfo.number = 5336054;

        if (ProfInfo.name.indexOf ("Prof.") != -1)
        {
            c.println (ProfInfo.name
                    + " is a prof - and his phone number is "
                    + ProfInfo.number);
        }

        // 2. Example of an entire phonebook as an array of records

        final int maxEntries = 100;
        int nEntries = 0;

        // In Java, an array of records is created by first creating
        // the array, and then all the records in it

        PhoneEntry PhoneBook [] = new PhoneEntry [maxEntries];
        for (int i = 0 ; i < maxEntries ; i++)
        {
            PhoneBook [i] = new PhoneEntry ();
        }

        // Once created, the array of records can be used easily

        c.println ("Enter name and number pairs, end with \"end\"");

        for (int i = 0 ; i < maxEntries ; i++)
        {
            // Read in each entry of the phonebook
            String name = c.readString ();

            // Done when the entry says "end"
            if (name.equals ("end"))
                break;

            // Otherwise enter the name and number,
            // and count one more entry
            PhoneBook [i].name = name;
            PhoneBook [i].number = c.readInt ();
            nEntries += 1;
        }

        c.println ("Thanks - now you can use the phone book");

        // Now we can use the array to look for things

        while (true)
        {
            // Get name to look for
            c.print ("Enter name to look for (\"end\" to exit): ");
            String name = c.readLine ();

            // Done at "end"
            if (name.equals ("end"))
                break;

            // Find the name
            int i;
            for (i = 0 ; i < nEntries - 1; i++)
            {
                if (PhoneBook [i].name.equals (name))
                    break;
            }

            // See if we found it
            if (PhoneBook [i].name.equals (name))
            {
                c.println ("The number is: " + PhoneBook [i].number);
            }
            else
            {
                c.println ("Name not found");
            }
        }

        c.println ("Bye!");

    } // main method
} // PhoneRecords class
