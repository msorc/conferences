TXL Generic Program

ToHTML.Txl	- generic program to convert any programming language source 
			  to a colorized HTML page

CppToHTML.Txl	- convert raw C++ source program to colorized HTML page
JavaToHTML.Txl	- convert raw Java source program to colorized HTML page

Usage:  	txl myprog.cpp CppToHTML.Txl > myprog.html
				(and similarly for other languages)

Input:  	myprog.cpp		Any raw source C++ program

Output: 	myprog.html 	Colorized HTML page of the program

This generic program will mark up source programs in any language,
given the language's TXL grammar, including formatting, keywords,
comments and token definitions.  It works for both raw source (text line)
grammars and formatting (pretty-printing) grammars.

This partial program should be used by a main program of the form:

	include "Language.Grammar"
	include "ToHTML.Txl"

where "Language.Grammar" is the grammatical specification of the
input language to be colorized.  See CppToHTML.Txl and the other
language examples for example use.

Requires:	
	HTML.Module		TXL HTML markup module

Examples:
	txl Examples/groff.cpp CppToHTML.Txl > groff.html
	txl Examples/PhoneRecords.java JavaToHTML.Txl > PhoneRecords.html
	txl Examples/ToHTML.Txl	TxlToHTML.Txl > ToHTML.html
	
	The output should be viewed using a browser such as Netscape or MIE.
	Due to bugs in MIE, line spacing may appear incorrect when viewed using
	MIE.  Netscape gets it right.
	
JRC June 2001
