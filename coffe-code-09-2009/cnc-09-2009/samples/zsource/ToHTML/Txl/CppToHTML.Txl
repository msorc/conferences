% Convert raw C++ source lines to colorized HTML
% J.R. Cordy, TSRI, June 2001

% C++ lexical tokens, keywords and commenting conventions
include "Cpp.Lexemes"

% Grammar for raw text lines of any language
include "Textlines.Grammar"

% Generic HTML markup program
include "ToHTML.Txl"
