TXL Example Specification
-------------------------

Name:        
        ctrace.Txl

Description:
        A general paradigm for transforming programs in any language
        to self-tracing versions of themselves.  Examples here are
        pure Turing, Standard Pascal and ANSI C.

Author:
        J.R. Cordy

Affiliation:
        Software Technology Laboratory
        Queen's University, Kingston, Canada

Date:
        Sept 1993

Examples:
        txl stack.c ctrace.Txl
