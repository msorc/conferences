Simple example annotating C++ code with XML markup
J.R. Cordy, April 2006

Intended as an example of the technique for annotating
code using TXL.  Annotates all functions in a C++ program 
with XML markup giving their name and type, as in:

      <function name="foo" type="char *">
      char * foo (int bar, blat)
      {
          boolean boo
          boo=0;
          if (bar == blat) boo=1;
      }
      </function>

Based on the C++ grammar from the TXL website.

Example:
	txl Examples/groff.cpp cppAnnotate.Txl

Rev 8.4.06
